# **How to deploy a Java Project**

## **Requirements**

- [Instalar Java](http://tecadmin.net/install-oracle-java-8-jdk-8-ubuntu-via-ppa/)

- [Instalar Glassfish](https://docs.oracle.com/cd/E19798-01/821-1757/6nmni99aj/index.html)
```
wget http://download.java.net/glassfish/4.1.1/release/glassfish-4.1.1.zip
unzip glassfish-4.1.1.zip
```

- [MYSQL](https://www.cyberciti.biz/faq/howto-install-mysql-on-ubuntu-linux-16-04/)

### Configuración MySql

``` sql
set global sql_mode='STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION';
set session sql_mode='STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION';
```

## **Before to...**

### Let's assume that the Glassfish is in **/root/glassfish4**

- How to enter inside **Asadmin**
``` 
/root/glassfish4/bin/asadmin
```

## **Basic operations**

- Start Glassfish
``` 
/root/glassfish4/bin/asadmin start-domain
``` 

- Stop Glassfish
``` 
/root/glassfish4/bin/asadmin stop-domain
``` 

- Restart Glassfish
``` 
/root/glassfish4/bin/asadmin restart-domain
```

- List apps
``` 
/root/glassfish4/bin/asadmin list-applications
```

## **Deployment**

- Deploy a .war
``` 
/root/glassfish4/bin/asadmin deploy path/to/war.war
```

- You can do an automatic deploy by copying the .war file in
``` 
/root/glassfish4/glassfish/domains/domain1/autodeploy
``` 

## **Log file in real time**

- See Glassfish logs.
``` 
tail -f glassfish4/glassfish/domains/domain1/logs/server.log
```